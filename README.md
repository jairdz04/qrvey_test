##Api documentation

```
git clone https://bitbucket.org/jairdz04/qrvey_test
cd qrvey_test
copy the .env file
npm install
node server.js
```

##Description
The idea is to develop an app that allows to take tracking of times (similar to Toggl.com)
base url : http://127.0.0.1:3000/
Run application: node server.js


##Projects

/project , returns an array of objects with properties as a: id_project [any], name[String], description[String]  

/project/:id , returns an especific project

/project/add, Save a new project

/project/edit/:id, Allow edit an existing project

/project/delete/:id, Allow delete an existing project


##Tasks

```
  NOTE: the id_task is a numeric number created based on the date and time of the record. In that sense we can sort from more recent to the oldest just using the id_task. But, why in that way? It´s easier to use an id to make the basical operations on a record (create, read,update,delete) and i needed the guarantee that the id will not be repeated. 
```

/task , returns an array of objects with properties as a: id_task [any], time_record[object], name[String], date[string], finish[number between 0 and 1],description[String]  

/task-by-order, returs an array of objects sorted from the most recent to the oldest

/task-by-day ,returs an array of objects ordered by days

/task/:id , returns an especific task

/task/add, Save a new task

/task/edit/:id, Allow edit an existing task

/task/delete/:id, Allow delete an existing task



##Time records


/time-record , returns an array of objects with properties as a: id_time_record [any],hours[number],minutes[number], seconds[number] 

/time-record/:id , returns an especific time record

/time-record/add, Save a new time record

/time-recordt/edit/:id, Allow edit an existing time record

/time-record/delete/:id, Allow delete an existing time record



##Relation between projects and tasks


/project-task , returns an array of objects with specific properties as a: id_project[any], id_task[any]

/project-task/add, Save a new relation between projects and tasks

/project-task/delete/:id, Allow delete an existing project-task

##Auth - through Twitter

/twitter/login 

/access-token , callback from twitter

```
Unit test
File: test.js
Excecute: npm test
```


