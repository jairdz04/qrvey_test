const fs = require("fs");
const identifier = require("../helpers/identifier"); 
const verify = require("../helpers/verifyKeys");
const { PROJECT_FILE } = process.env; 


exports.getAllProjects = async (req, res)=>{
	try{

		let data = await fs.readFileSync(PROJECT_FILE);
		data = JSON.parse(data);
		const response = data.filter(d=> d.status === 1);
		return res.status(200).json(response);
		
	}catch(err){
		return res.status(500).json({details: "Something went wrong"});
	}
};

exports.getProjectById = async (req, res)=>{
	try{
		const id_project = parseInt(req.params.id);
		let data = await fs.readFileSync(PROJECT_FILE);
		data = JSON.parse(data);
		const index = data.findIndex(i=> i.id_project === id_project);
		if(index > -1) return res.status(200).json(data[index]);
		return res.status(200).json({});
			
	}catch(err){
		return res.status(500).json({details: "Something went wrong"});
	}
};

exports.postProject = async (req, res)=>{
	try{
		let data = await fs.readFileSync(PROJECT_FILE);
		data = JSON.parse(data);			
		let params = req.body;
		params.id_project =  parseInt(identifier.getIdentifier());
		params.status = 1;
		data.push(params);
		const saved = await fs.writeFileSync(PROJECT_FILE,JSON.stringify(data));
		return res.status(200).json({details: "Saved correctly"});
		
	}catch(err){
		return res.status(500).json({details: "Something went wrong"});
	}
};

exports.putProject = async (req, res)=>{
	try{
		let data = await fs.readFileSync(PROJECT_FILE);
		data = JSON.parse(data);

		const params = req.body;
		const id_project = parseInt(req.params.id);
		const index = data.findIndex(d => d.id_project === id_project);
		if(index == -1) return res.status(400).json({details : "Project does not exist"});
		const update_project = {
			id_project: data[index].id_project,
			name: params.name || data[index].name,
			description: params.description || data[index].description,
			status: data[index].status
		};
		data[index] = update_project;
		const updated = await fs.writeFileSync(PROJECT_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Updated correctly"});

	}catch(err){
		return res.status(500).json({details: "Something went wrong"});
	}
};

exports.deleteProject = async (req, res)=>{
	try{
		let data = await fs.readFileSync(PROJECT_FILE);
		data = JSON.parse(data);

		const id_project = parseInt(req.params.id);
		const index = data.findIndex(d => d.id_project === id_project);
		if(index == -1) return res.status(400).json({details : "Project does not exist"});
		data[index].status = 0;
		const deleted = await fs.writeFileSync(PROJECT_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Deleted correctly"});
		
	}catch(err){
		return res.status(500).json({details: "Something went wrong"});
	}
};