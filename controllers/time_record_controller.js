const fs = require("fs");
const identifier = require("../helpers/identifier");
const { TIME_FILE } = process.env; 

exports.getAllTimesRecords = async (req, res)=>{
	try{

		let data = await fs.readFileSync(TIME_FILE);
		data = JSON.parse(data);
		return res.status(200).json(data);

	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};

exports.getTimeRecordById = async (req, res)=>{
	try{
		const id_time_record = parseInt(req.params.id);
		let data = await fs.readFileSync(TIME_FILE);
		data = JSON.parse(data);
		const index = data.findIndex(i=> i.id_time_record === id_time_record);
		if(index > -1) return res.status(200).json(data[index]);
		return res.status(200).json({});

		
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};

exports.postTimeRecord = async (req, res)=>{
	try{
		let data = await fs.readFileSync(TIME_FILE);
		data = JSON.parse(data);			
		let params = req.body;
		params.id_time_record =  parseInt(identifier.getIdentifier());
		data.push(params);
		const saved = await fs.writeFileSync(TIME_FILE,JSON.stringify(data));
		return res.status(200).json({details: "Saved correctly"});
	
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};

exports.putTimeRecord = async (req, res)=>{
	try{

		let data = await fs.readFileSync(TIME_FILE);
		data = JSON.parse(data);
		const params = req.body;
		const id_time_record = parseInt(req.params.id);
		const index = data.findIndex(d => d.id_time_record === id_time_record);
		if(index == -1) return res.status(400).json({details : "Time record does not exist"});
		const update_time = {
			id_time_record: data[index].id_time_record,
			hours: parseInt(params.hours),
			minutes: parseInt(params.minutes),
			seconds: parseInt(params.seconds)
		};
		data[index] = update_time;
		const updated = await fs.writeFileSync(TIME_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Updated correctly"});

	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};
