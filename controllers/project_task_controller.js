const fs = require("fs");
const verify = require("../helpers/verifyKeys");
const time = require("../helpers/times");
const { PROJECT_TASK_FILE } = process.env; 


exports.getAllRelations = async (req, res)=>{
	try{

		let data = await fs.readFileSync(PROJECT_TASK_FILE);
		data = JSON.parse(data);
		const get_times = data.filter(d=> d.status === 1);
		const response = await time.getTimes(get_times);
		return res.status(200).json(response);

	}catch(err){
		return res.status(500).json({err : "Something went wrong"});		
	}
};

exports.postRelation = async (req, res)=>{
	try{
		let data = await fs.readFileSync(PROJECT_TASK_FILE);
		data = JSON.parse(data);
		let params = req.body;
		const pj = await verify.project(params.id_project);
		if(!pj) return res.status(404).json({details: "Project does not exist!"});
		const tk = await verify.task(params.id_task);
		if(!tk) return res.status(404).json({details: "Task does not exist!"});
		params.status = 1;
		data.push(params);
		const saved = await fs.writeFileSync(PROJECT_TASK_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Saved correctly"});
		
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});		
	}
};

exports.deleteRelation = async (req, res)=>{
	try{
		let data = await fs.readFileSync(PROJECT_TASK_FILE);
		data = JSON.parse(data);
		const {id_project, id_task} = req.body;

		const index = data.findIndex(d => (d.id_project === parseInt(id_project)) && (d.id_task === parseInt(id_task)));
		if(index == -1) return res.status(400).json({details : "Relation does not exist"});
		data[index].status = 0;
		const deleted = await fs.writeFileSync(PROJECT_TASK_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Deleted correctly"});
		
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};