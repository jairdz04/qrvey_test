const fs = require ("fs");
const identifier = require("../helpers/identifier"); 
const order = require("../helpers/orderData")
const verify = require("../helpers/verifyKeys");
const { TASK_FILE, TIME_FILE } = process.env; 


exports.getAllTasks = async (req, res)=>{
	try{
		let times = await fs.readFileSync(TIME_FILE);
		times = JSON.parse(times);

		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);
		const response = data.filter(d=> d.status === 1);
		const all = response.map(i=> {
			const t = times.filter(t=> t.id_time_record === i.id_time_record);
			return {
				id_task: i.id_task,
				name: i.name,
				date: i.date,
				finish: i.finish,
				description: i.description,
				time_record: t[0],				
				status: i.status
			};
		});

		return res.status(200).json(all);

	}catch(err){
		console.log(err);
		return res.status(500).json({err : "Something went wrong"});
	}
}

exports.getTaskById = async (req, res)=>{
	try{
		
		const id_task = parseInt(req.params.id);	
		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);
		const index = data.findIndex(i=> i.id_task === id_task);
		if(index > -1) return res.status(200).json(data[index]);
		return res.status(200).json({});

	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};

exports.getRecentToOldestTask = async (req, res)=>{
	try{

		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);
		const tasks = await order.orderChronologically(data);
		if(tasks instanceof Array) return res.status(200).json(tasks);
		return res.status(400).json(tasks);

	}catch(err){
		return res.status(500).json({err : "Something went wrong"});		
	}
};

exports.getTaskGroupByDay = async (req, res)=>{
	try{
		
		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);
		const tasks = await order.orderByDay(data);
		if(tasks instanceof Array) return res.status(200).json(tasks);
		return res.status(400).json(tasks);

	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};

exports.postTask = async (req, res)=>{
	try{

		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);			
		let params = req.body;
		const id_time_record = params.id_time_record;
		const flag = await verify.time_record(id_time_record);
		if(flag){
			params.id_task =  parseInt(identifier.getIdentifier());
			params.id_time_record = parseInt(params.id_time_record);
			params.status = 1;
			data.push(params);
			const saved = await fs.writeFileSync(TASK_FILE,JSON.stringify(data));
			return res.status(200).json({details: "Saved correctly"});
		}else{
			return res.status(400).json({err : "Time record does not exist"});		
		}
		
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}

};

exports.putTask = async (req, res)=>{
	try{

		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);
		const params = req.body;
		const id_task = parseInt(req.params.id);
		const index = data.findIndex(d => d.id_task === id_task);
		if(index == -1) return res.status(400).json({details : "Task does not exist"});
		const update_task = {
			id_task: data[index].id_task,
			id_time_record: params.id_time_record || data[index].id_time_record,
			name: params.name || data[index].name,
			date: params.date || data[index].date,
			finish: parseInt(params.finish),
			description: params.description || data[index].description,
			status: data[index].status
		};
		const vr = await verify.time_record(update_task.id_time_record);
		if(!vr) return res.status(400).json({details : "Time record  does not exist"}); 
		data[index] = update_task;
		const updated = await fs.writeFileSync(TASK_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Updated correctly"});
		
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}


};

exports.deleteTask = async (req, res)=>{
	try{
		let data = await fs.readFileSync(TASK_FILE);
		data = JSON.parse(data);

		const id_task = parseInt(req.params.id);
		const index = data.findIndex(d => d.id_task === id_task);
		if(index == -1) return res.status(400).json({details : "Task does not exist"});
		data[index].status = 0;
		const deleted = await fs.writeFileSync(TASK_FILE, JSON.stringify(data));
		return res.status(200).json({details: "Deleted correctly"});
		
	}catch(err){
		return res.status(500).json({err : "Something went wrong"});
	}
};