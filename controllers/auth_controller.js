const co = require("co");
const auth = require("../middlewares/auth");

exports.error = async (req,res)=>{
	try{
		res.status(400).json({details: "Not authenticated!"})
	}catch(err){
		return res.status(500).json({details: "Something went wrong"});		
	}
};
