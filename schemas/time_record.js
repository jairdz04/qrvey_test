const joi = require("joi");

module.exports = {
	time_record: {
		body: {
			id_time_record: joi.any(),
			hours: joi.number().integer().required(),
			minutes: joi.number().integer().min(0).max(59).required(),
			seconds: joi.number().integer().min(0).max(59).required()			
		}
	}
}
