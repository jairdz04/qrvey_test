module.exports = {
	getIdentifier: ()=>{
		const currentdate = new Date(); 
		const id = currentdate.getFullYear() + ""  
					 + (currentdate.getMonth()+1)  + "" 
					 +  currentdate.getDate() + ""
					 + currentdate.getHours() + ""  
					 + currentdate.getMinutes() + "" 
					 + currentdate.getSeconds();
		return id;
	}
}

 