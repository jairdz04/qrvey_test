module.exports = {
	orderChronologically: async (data)=>{
		try{
			let finished = [];
			if(data.length > 0){
				for(var i in data){
					if(data[i].finish == 1){
						finished.push(data[i]);
					}
				}
				finished.sort((a,b)=>{
					return a.id_task - b.id_task;
				});
				return finished;

			}else{
				return {error: "Nothing to sort"};
			}
		}catch(err){
			return err;
		}	
	},
	orderByDay: async (data)=>{
		try{
			var date = "";
			var done = [];
			var records = [];
			var finalArray = [];
			if(data.length > 0){
				for(var i in data){
				records = [];
				date = data[i].date;
					if(done.indexOf(date) == -1){
						for(var j in data){
							if(date == data[j].date){
								records.push(data[j]);
							}
						}
						done.push(date);
						if(records.length > 0){
							finalArray.push({ date : date, records : records});
						}
					}
				}
				return finalArray;
			
			}else{
				return {error: "Nothing to sort"};
			}
				
		}catch(err){
			return err;
		}
	}
}

 