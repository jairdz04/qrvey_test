const fs = require('fs');
const { TIME_FILE, PROJECT_FILE , TASK_FILE } = process.env; 

module.exports = {
	time_record: async (id_time_record)=>{
		try{

			let data = await fs.readFileSync(TIME_FILE);
			data = JSON.parse(data);
			const exist = data.findIndex(d=> d.id_time_record === parseInt(id_time_record));
			if(exist > -1) return true;
			return false;

		}catch(err){
			return err;
		}
	},
	project: async (id_project)=>{
		try{

			let data = await fs.readFileSync(PROJECT_FILE);
			data = JSON.parse(data);
			const exist = data.findIndex(d=> d.id_project === parseInt(id_project));
			if(exist > -1) return true;
			return false;

		}catch(err){
			return err;
		}

	},
	task: async (id_task)=>{
		try{

			let data = await fs.readFileSync(TASK_FILE);
			data = JSON.parse(data);
			const exist = data.findIndex(d=> d.id_task === parseInt(id_task));
			if(exist > -1) return true;
			return false;

		}catch(err){
			return err;
		}

	}
}

 