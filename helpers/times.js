const fs = require("fs");
const { TIME_FILE, PROJECT_FILE , TASK_FILE } = process.env; 

module.exports = {
	getTimes: async (data)=>{
        try{
            
            let projects = await fs.readFileSync(PROJECT_FILE);
                projects = JSON.parse(projects);
            let tasks = await fs.readFileSync(TASK_FILE);
                tasks = JSON.parse(tasks);
            let times = await fs.readFileSync(TIME_FILE);
                times = JSON.parse(times);

			var id_project = "";
            var done = [];
			var _tasks = [];
            var finalArray = [];

			if(data.length > 0){
				for(var i in data){
                _tasks = [];
                let hours_project = 0;
                let minutes_project = 0;
                let seconds_project = 0;
				id_project = parseInt(data[i].id_project);
					if(done.indexOf(id_project) == -1){
                        const pr = projects.filter(pj => pj.id_project == id_project)
						for(var j in data){
							if(id_project == data[j].id_project){
                                const tk = parseInt(data[j].id_task);
                                const task_info = tasks.filter(tsk => tsk.id_task === tk);
                                const time = times.filter(tms => tms.id_time_record === parseInt(task_info[0].id_time_record));
                                hours_project += time[0].hours;
                                minutes_project += time[0].minutes;
                                seconds_project += time[0].seconds;
								_tasks.push({
                                    id_task: task_info[0].id_task,
                                    name: task_info[0].name,
                                    description: task_info[0].description,
                                    date: task_info[0].date,
                                    finish: task_info[0].finish,
                                    status: task_info[0].status,
                                    time: time[0]
                                });
							}
                        }

                        done.push(id_project);    
                                     
						if(_tasks.length > 0){
                            const pro = {
                                id_project: pr[0].id_project,
                                name: pr[0].name,
                                description: pr[0].description,
                                hours_project: hours_project,
                                minutes_project: minutes_project,
                                seconds_project: seconds_project,
                                tasks: _tasks,
                                status: pr[0].status,
                            }   
							finalArray.push(pro);
						}
					}
				}
				return finalArray;
            }
            
            return [];

        }catch(err){
			return err;
        }
    }
}

 