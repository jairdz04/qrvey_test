module.exports = (app) =>{
    require("./routes/auth")(app);    
    require("./routes/project")(app);
    require("./routes/task")(app);
    require("./routes/time")(app);
    require("./routes/project_task")(app);
};