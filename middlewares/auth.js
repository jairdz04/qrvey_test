const passport = require("passport");
const tw_strategy = require("passport-twitter").Strategy;
const { CONSUMER_KEY,CONSUMER_SECRET,CALLBACK_URL } = process.env

module.exports = {
	auth: ()=>{
		passport.use(new tw_strategy({
			consumerKey: CONSUMER_KEY,
			consumerSecret: CONSUMER_SECRET,
			callback: CALLBACK_URL
		}, (token, tokenSecret, user, callback)=>{
				return callback(null,user);
		}));

		passport.serializeUser((user, callback)=>{
			callback(null, user);
		});

		passport.deserializeUser((obj, callback)=>{
			callback(null, obj);			
		});
	}
}