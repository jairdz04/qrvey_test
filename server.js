require('dotenv').config();
const express = require("express");
const app = express();
const passport = require('passport');
const session = require('express-session');
const bodyParser = require("body-parser");
const expressValidation = require("express-validation");
const routes = require("./routing");
const cors = require("cors");
const port = Number(process.env.PORT || 3000);

app.use(cors({ origin: "*", optionSuccessStatus: 200 }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(session({
  secret: process.env.SECRET_KEY,
  resave: true,
  saveUninitialized: true
}));

const auth = require("./middlewares/auth").auth();
app.use(passport.initialize());
app.use(passport.session());


app.use((err, req, res, next) => {
  if (err instanceof expressValidation.ValidationError) {
    res.status(err.status).json(err);
  } else {
    res.status(500).json({ status: err.status, message: err.message });
  }
});

routes(app);

app.listen(port, () => {
	console.log('server listening on', port);
});

module.exports = app;