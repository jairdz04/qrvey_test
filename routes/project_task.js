const validate = require("express-validation");
const project_task = require('../controllers/project_task_controller');
const sch_project_task = require('../schemas/project_task');

module.exports = (router) => {

    router.get('/project-task', project_task.getAllRelations);
    router.post('/project-task/add',validate(sch_project_task.project_task),project_task.postRelation);
    router.delete('/project-task/delete/:id',project_task.deleteRelation);

}