const validate = require("express-validation");
const project = require('../controllers/project_controller');
const sch_project = require('../schemas/project');


module.exports = (router) => {

    router.get('/project', project.getAllProjects);
    router.get('/project/:id', project.getProjectById);
    router.post('/project/add' , validate(sch_project.project),project.postProject);
    router.put('/project/edit/:id', validate(sch_project.project), project.putProject);
    router.delete('/project/delete/:id', project.deleteProject);

}