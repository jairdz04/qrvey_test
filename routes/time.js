const validate = require("express-validation");
const time_record = require('../controllers/time_record_controller');
const sch_time = require('../schemas/time_record');

module.exports = (router) => {

    router.get('/time-record', time_record.getAllTimesRecords);
    router.get('/time-record/:id', time_record.getTimeRecordById);
    router.post('/time-record/add',validate(sch_time.time_record),time_record.postTimeRecord);
    router.put('/time-record/edit/:id',time_record.putTimeRecord);

}