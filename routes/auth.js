const passport = require("passport");
const validate = require("express-validation");
const auth = require('../controllers/auth_controller');

module.exports = (router)=>{
    router.get('/twitter/login', passport.authenticate('twitter'));
    router.get('/error', auth.error);
    router.get('/api/access-token', passport.authenticate('twitter', {
        failureRedirect: '/error'
    }), (req, res) => {
        res.redirect('/project-task');
    })
}