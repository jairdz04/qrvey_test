const validate = require("express-validation");
const task = require('../controllers/task_controller');
const sch_task = require('../schemas/task');


module.exports = (router) => {

    router.get('/task',task.getAllTasks);
    router.get('/task-by-order', task.getRecentToOldestTask);
    router.get('/task-by-day', task.getTaskGroupByDay);
    router.get('/task/:id',task.getTaskById);
    router.post('/task/add',validate(sch_task.task) ,task.postTask);
    router.put('/task/edit/:id',task.putTask);
    router.delete('/task/delete/:id', task.deleteTask);

}